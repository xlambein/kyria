#ifndef FRENCH_H
#define FRENCH_H

// Unicode stuff ruthlessly stolen from Yannick Jamet:
// https://github.com/yannickjmt/qmk_firmware/blob/yannick-layout-nomac/keyboards/xd75/keymaps/yannick/keymap.c
enum unicode_names {
  E_AIG,
  E_AIG_MAJ,
  E_GRV,
  E_GRV_MAJ,
  E_CIRC,
  E_CIRC_MAJ,
  E_TREMA,
  E_TREMA_MAJ,
  A_CIRC,
  A_CIRC_MAJ,
  A_GRV,
  A_GRV_MAJ,
  C_CEDILLE,
  C_CEDILLE_MAJ,
  O_CIRC,
  O_CIRC_MAJ,
  U_GRV,
  U_GRV_MAJ,
  OE,
  OE_MAJ,
  ESZETT,

  PI,
  MU,
  LAMBDA,
  LAMBDA_MAJ,
  SIGMA,

  EURO,
  CARRE,
  CUBE,
  DEGREE,

  COPYRIGHT,
  REGISTERED,
  INFEQ,
  SUPEQ,
  GUILL_G,
  GUILL_R,
  UNEQUAL,
  PRETTYMUCH,
  INFINIT,
};

const uint32_t PROGMEM unicode_map[] = {
  [E_AIG]  = 0x00E9,  // 0 é
  [E_AIG_MAJ] = 0x00C9,  // 1 É
  [E_GRV]  = 0x00E8,  // 2 è
  [E_GRV_MAJ] = 0x00C8,  // 3 È
  [E_CIRC]  = 0x00EA,  // 4 ê
  [E_CIRC_MAJ] = 0x00CA,  // 5 Ê
  [E_TREMA]  = 0x00EB,  // 6 ë
  [E_TREMA_MAJ] = 0x00CB,  // 7 Ë
  [A_CIRC]  = 0x00E2,  // 8 â
  [A_CIRC_MAJ] = 0x00C2,  // 9 Â
  [A_GRV]  = 0x00E0,  // 10 à
  [A_GRV_MAJ] = 0x00C0,  // 11 À
  [C_CEDILLE]  = 0x00E7,  // 12 ç
  [C_CEDILLE_MAJ] = 0x00C7,  // 13 Ç
  [O_CIRC]  = 0x00F4,  // 22 ô
  [O_CIRC_MAJ] = 0x00D4,  // 23 Ô
  [U_GRV]  = 0x00F9,  // 24 ù
  [U_GRV_MAJ] = 0x00D9,  // 25 Ù
  [OE]  = 0x0153,  // 26 œ
  [OE_MAJ] = 0x0152,  // 27 Œ
  [ESZETT] = 0x00DF,  // 27 ß
  [PI] = 0x03C0, // 28 π
  [LAMBDA] = 0x03BB, // λ
  [LAMBDA_MAJ] = 0x039B, // Λ
  [MU]  = 0x00B5,  // 32 µ
  [SIGMA] = 0x03C3,  // 33 σ
  [EURO]  = 0x20AC,  // 16 €
  [CARRE]  = 0x00B2,  // 14 ²
  [CUBE] = 0x00B3,  // 15 ³
  [DEGREE] = 0x00B0, // 29 °
  [INFEQ]  = 0x2264,  // 34 ≤
  [SUPEQ] = 0x2265,  // 35 ≥
  [GUILL_G]  = 0x00AB,  // 36 «
  [GUILL_R] = 0x00BB,  // 37 »
  [UNEQUAL]  = 0x2260,  // 38 ≠
  [PRETTYMUCH] = 0x2248,  // 39 ≈
  [INFINIT] = 0x221E,  // 40 ∞
};

#define FR_EAIG	UP(E_AIG, E_AIG_MAJ)
#define FR_EGRV	UP(E_GRV, E_GRV_MAJ)
#define FR_ECRC	UP(E_CIRC, E_CIRC_MAJ)
#define FR_ETRM	UP(E_TREMA, E_TREMA_MAJ)
#define FR_ACRC	UP(A_CIRC, A_CIRC_MAJ)
#define FR_AGRV	UP(A_GRV, A_GRV_MAJ)
#define FR_CCDL	UP(C_CEDILLE, C_CEDILLE_MAJ)
#define FR_OCRC	UP(O_CIRC, O_CIRC_MAJ)
#define FR_UGRV	UP(U_GRV, U_GRV_MAJ)
#define FR_OE	UP(OE, OE_MAJ)
#define FR_PI	UM(PI)
#define FR_LMBD	UP(LAMBDA, LAMBDA_MAJ)
#define FR_MU	UM(MU)
#define FR_SIGM	UM(SIGMA)
#define FR_EURO	UM(EURO)
#define FR_ESZT	UM(ESZETT)

#endif

