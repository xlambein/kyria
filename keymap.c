#include QMK_KEYBOARD_H

#include "keymap_dvorak.h"
#include "sendstring_dvorak.h"
#include "french.h"
// #include "leader.h"

// Ideas of things to add:
// - https://docs.qmk.fm/#/feature_macros?id=super-alt%e2%86%aftab
// - 

#define HM_A MT(MOD_LGUI, DV_A)
#define HM_O MT(MOD_LALT, DV_O)
#define HM_E MT(MOD_LSFT, DV_E)
#define HM_U MT(MOD_LCTL, DV_U)
#define HM_H MT(MOD_RCTL, DV_H)
#define HM_T MT(MOD_RSFT, DV_T)
#define HM_N MT(MOD_LALT, DV_N)
#define HM_S MT(MOD_RGUI, DV_S)

#define HM_F2 MT(MOD_LGUI, KC_F2)
#define HM_F3 MT(MOD_LALT, KC_F3)
#define HM_F4 MT(MOD_LSFT, KC_F4)
#define HM_F5 MT(MOD_LCTL, KC_F5)

#define HM_LEFT MT(MOD_LCTL, KC_LEFT)
#define HM_UP   MT(MOD_LSFT, KC_UP)
#define HM_RGHT MT(MOD_LALT, KC_RGHT)

enum layers {
    _DVORAK = 0,
    _GAMING,
    _LOWER,
    _RAISE,
    _FRENCH
};

enum custom_keycodes {
    KC_CCCV = SAFE_RANGE,
};

#define CCCV LT(0, KC_CCCV)

const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {
/*
 * Base Layer: Dvorak
 *
 * ,-------------------------------------------.                              ,-------------------------------------------.
 * |  ESC   | ' "  | , <  | . >  |   P  |   Y  |                              |   F  |   G  |   C  |   R  |   L  |  / ?   |
 * |--------+------+------+------+------+------|                              |------+------+------+------+------+--------|
 * |  TAB   |   A  |   O  |   E  |   U  |   I  |                              |   D  |   H  |   T  |   N  |   S  |  Bspc  |
 * |--------+------+------+------+------+------+-------------.  ,-------------+------+------+------+------+------+--------|
 * | LShift | ; :  |   Q  |   J  |   K  |   X  |CAPSWD| CCCV |  |LShift|LShift|   B  |   M  |   W  |   V  |   Z  | RShift |
 * `----------------------+------+------+------+------+------|  |------+------+------+------+------+----------------------'
 *                        | Caps | Esc  | Space| Esc  | Gui  |  | Gui  | Bspc | Enter|      |Gaming|
 *                        |      |      | Lower| Raise| LCtl |  | LCtl | Lower| Raise| Bspc |      |
 *                        `----------------------------------'  `----------------------------------'
 *                         
 */
    [_DVORAK] = LAYOUT(
      KC_NO  , DV_QUOT, DV_COMM, DV_DOT , DV_P   , DV_Y   ,                                     DV_F   , DV_G   , DV_C   , DV_R   , DV_L   , KC_NO,
      KC_TAB , HM_A   , HM_O   , HM_E   , HM_U   , DV_I   ,                                     DV_D   , HM_H   , HM_T   , HM_N   , HM_S   , KC_NO,
      KC_NO  , DV_SCLN, DV_Q   , DV_J   , DV_K   , DV_X   , CW_TOGG, CCCV   , KC_LSFT, KC_LSFT, DV_B   , DV_M   , DV_W   , DV_V   , DV_Z   , KC_NO,
                                 KC_CAPS, KC_ESC , LT(_LOWER, KC_SPC), LT(_RAISE, KC_ESC), MT(MOD_LCTL, KC_LGUI), MT(MOD_LCTL, KC_LGUI), LT(_LOWER, KC_BSPC), LT(_RAISE, KC_ENT),  KC_BSPC, TG(_GAMING)
    ),
/*
 * Lower Layer: Symbols
 *
 * ,-------------------------------------------.                              ,-------------------------------------------.
 * |        |  !   |  @   |  {   |  }   |  |   |                              |      |  _   |  \   |  ?   |      |        |
 * |--------+------+------+------+------+------|                              |------+------+------+------+------+--------|
 * |        |  #   |  $   |  (   |  )   |  `   |                              |   +  |  -   |  /   |  *   |  %   |        |
 * |--------+------+------+------+------+------+-------------.  ,-------------+------+------+------+------+------+--------|
 * |        |      |  ^   |  [   |  ]   |  ~   |      |      |  |      |      |   &  |  =   |      |      |      |        |
 * `----------------------+------+------+------+------+------|  |------+------+------+------+------+----------------------'
 *                        |      |      |      |      |      |  |      |      |      |      |      |
 *                        |      |      |      |      |      |  |      |      |      |      |      |
 *                        `----------------------------------'  `----------------------------------'
 */ 
		[_LOWER] = LAYOUT(
		  XXXXXXX, DV_EXLM, DV_AT  , DV_LCBR, DV_RCBR, DV_PIPE,                                     XXXXXXX, DV_UNDS, DV_BSLS, DV_QUES, XXXXXXX, XXXXXXX,
		  XXXXXXX, DV_HASH, DV_DLR , DV_LPRN, DV_RPRN, DV_GRV ,                                     DV_PLUS, DV_MINS, DV_SLSH, DV_ASTR, DV_PERC, XXXXXXX,
		  XXXXXXX, XXXXXXX, DV_CIRC, DV_LBRC, DV_RBRC, DV_TILD, _______, _______, _______, _______, DV_AMPR, DV_EQL , XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX,
									               _______, _______, _______, _______, _______, _______, _______, _______, _______, _______
	    ),
/*
 * Raise Layer: Number keys, media, navigation
 *
 * ,-------------------------------------------.                              ,-------------------------------------------.
 * |        |   1  |  2   |  3   |  4   |  5   |                              |  6   |  7   |  8   |  9   |  0   |        |
 * |--------+------+------+------+------+------|                              |------+------+------+------+------+--------|
 * | F1     | F2   | F3   | F4   | F5   | F6   |                              | Left | Down | Up   | Right| Play |        |
 * |--------+------+------+------+------+------+-------------.  ,-------------+------+------+------+------+------+--------|
 * | F7     | F8   | F9   | F10  | F11  | F12  |      |      |  |      |      | MLeft| Mdown| MUp  |MRight| Next | Prev   |
 * `----------------------+------+------+------+------+------|  |------+------+------+------+------+----------------------'
 *                        |      |      |      |      |      |  |      |      |      |      |      |
 *                        |      |      |      |      |      |  |      |      |      |      |      |
 *                        `----------------------------------'  `----------------------------------'
 */
    [_RAISE] = LAYOUT(
      _______, KC_1,    KC_2   , KC_3   , KC_4   , KC_5,                                        KC_6,    KC_7,    KC_8,    KC_9,    KC_0,    _______,
      KC_F1  , HM_F2  , HM_F3  , HM_F4  , HM_F5  , KC_F6  ,                                     KC_PGUP, HM_LEFT, HM_UP  , HM_RGHT, KC_MPLY, _______,
      KC_F7  , KC_F8  , KC_F9  , KC_F10 , KC_F11 , KC_F12 , _______, _______, _______, _______, KC_PGDN, KC_HOME, KC_DOWN, KC_END , KC_MNXT, KC_MPRV,
                                 _______, _______, _______, _______, _______, _______, _______, _______, _______, _______
    ),
 /*
  * French layer
  *
  * ,-------------------------------------------.                              ,-------------------------------------------.
  * |        |      |      |      |      |      |                              |      |      |      |      |      |        |
  * |--------+------+------+------+------+------|                              |------+------+------+------+------+--------|
  * |        |      |      |      |      |      |                              |      |      |      |      |      |        |
  * |--------+------+------+------+------+------+-------------.  ,-------------+------+------+------+------+------+--------|
  * |        |      |      |      |      |      |      |      |  |      |      |      |      |      |      |      |        |
  * `----------------------+------+------+------+------+------|  |------+------+------+------+------+----------------------'
  *                        |      |      |      |      |      |  |      |      |      |      |      |
  *                        |      |      |      |      |      |  |      |      |      |      |      |
  *                        `----------------------------------'  `----------------------------------'
  */
     [_FRENCH] = LAYOUT(
       _______, FR_ACRC, FR_ETRM, FR_EGRV, FR_PI,   _______,                                     _______, _______, FR_CCDL, _______, FR_LMBD, _______,
       _______, FR_AGRV, FR_OCRC, FR_EAIG, FR_UGRV, FR_EURO,                                     _______, _______, _______, _______, FR_ESZT, _______,
       _______, _______, FR_OE,   FR_ECRC, _______, _______, _______, _______, _______, _______, _______, FR_MU,   _______, _______, _______, _______,
                                  _______, _______, _______, _______, _______, _______, _______, _______, _______, _______
     ),
/*
 * Gaming hell yes
 *
 * ,-------------------------------------------.                              ,-------------------------------------------.
 * | Esc    |      | Q    |      | E    | R    |                              |      |      |      |      |      |        |
 * |--------+------+------+------+------+------|                              |------+------+------+------+------+--------|
 * | Tab    | Shft | Left | Up   | Right| F    |                              |      |      |      |      |      |        |
 * |--------+------+------+------+------+------+-------------.  ,-------------+------+------+------+------+------+--------|
 * |        |      |      | Down | C    |      |      |      |  |      |      |      |      |      |      |      |        |
 * `----------------------+------+------+------+------+------|  |------+------+------+------+------+----------------------'
 *                        |      |      | Spc  | Raise|      |  |      |      |      |      |      |
 *                        |      |      |      |      |      |  |      |      |      |      |      |
 *                        `----------------------------------'  `----------------------------------'
 */
    [_GAMING] = LAYOUT(
      KC_ESC , XXXXXXX, DV_Q   , DV_X   , DV_E   , DV_R   ,                                     XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX,
      KC_TAB , KC_LSFT, KC_LEFT, KC_UP  , KC_RGHT, DV_F   ,                                     XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX,
      XXXXXXX, KC_LCTL, XXXXXXX, KC_DOWN, DV_C   , DV_G   , XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX,
                                 XXXXXXX, XXXXXXX, KC_SPC , MO(_RAISE), XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, _______
    ),
// /*
//  * Layer template
//  *
//  * ,-------------------------------------------.                              ,-------------------------------------------.
//  * |        |      |      |      |      |      |                              |      |      |      |      |      |        |
//  * |--------+------+------+------+------+------|                              |------+------+------+------+------+--------|
//  * |        |      |      |      |      |      |                              |      |      |      |      |      |        |
//  * |--------+------+------+------+------+------+-------------.  ,-------------+------+------+------+------+------+--------|
//  * |        |      |      |      |      |      |      |      |  |      |      |      |      |      |      |      |        |
//  * `----------------------+------+------+------+------+------|  |------+------+------+------+------+----------------------'
//  *                        |      |      |      |      |      |  |      |      |      |      |      |
//  *                        |      |      |      |      |      |  |      |      |      |      |      |
//  *                        `----------------------------------'  `----------------------------------'
//  */
//     [_LAYERINDEX] = LAYOUT(
//       _______, _______, _______, _______, _______, _______,                                     _______, _______, _______, _______, _______, _______,
//       _______, _______, _______, _______, _______, _______,                                     _______, _______, _______, _______, _______, _______,
//       _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______,
//                                  _______, _______, _______, _______, _______, _______, _______, _______, _______, _______
//     ),
};

layer_state_t layer_state_set_user(layer_state_t state) {
    return update_tri_layer_state(state, _LOWER, _RAISE, _FRENCH);
}

uint8_t mod_state;
uint16_t cccv_timer;
// uint16_t key_timer;
bool process_record_user(uint16_t keycode, keyrecord_t *record) {
	mod_state = get_mods();
	switch(keycode) {
	case KC_BSPC:
    {
      // Initialize a boolean variable that keeps track
      // of the delete key status: registered or not?
      static bool delkey_registered;
      if (record->event.pressed) {
          // Detect the activation of either shift keys
          if (mod_state & MOD_MASK_SHIFT) {
              // First temporarily canceling both shifts so that
              // shift isn't applied to the KC_DEL keycode
              del_mods(MOD_MASK_SHIFT);
              register_code(KC_DEL);
              // Update the boolean variable to reflect the status of KC_DEL
              delkey_registered = true;
              // Reapplying modifier state so that the held shift key(s)
              // still work even after having tapped the Backspace/Delete key.
              set_mods(mod_state);
              return false;
          }
      } else { // on release of KC_BSPC
          // In case KC_DEL is still being sent even after the release of KC_BSPC
          if (delkey_registered) {
              unregister_code(KC_DEL);
              delkey_registered = false;
              return false;
          }
      }
      // Let QMK process the KC_BSPC keycode as usual outside of shift
      return true;
    }

	case CCCV:
    if (record->tap.count && record->event.pressed) {
        tap_code16(C(DV_V)); // Intercept hold function to send Ctrl-V
    } else if (record->event.pressed) {
        tap_code16(C(DV_C)); // Intercept tap function to send Ctrl-C
    }
		return false;
    break;
    
  // Fix for shift+bspc not working properly when using mod-tap
  case LT(_LOWER, KC_BSPC):
    if (record->tap.count && record->event.pressed) {
        return process_record_user(KC_BSPC, record);
    } else if (!record->event.pressed) {
        return process_record_user(KC_BSPC, record);
    }
    break;
	}
  
  // if (record->event.pressed && !handle_leader_key(keycode)) {
  //   return false;
  // }
  
	return true;
}

// Caps word adapted to Dvorak
bool caps_word_press_user(uint16_t keycode) {
    switch (keycode) {
				case DV_SCLN:
				case DV_QUOT:
				case DV_COMM:
				case DV_DOT:
						return false;
				default:;
		}
		
    switch (keycode) {
        // Keycodes that continue Caps Word, with shift applied.
        case KC_A ... KC_Z:
		    case DV_S:
			  case DV_W:
				case DV_V:
				case DV_Z:
            add_weak_mods(MOD_BIT(KC_LSFT));  // Apply shift to next key.
            return true;

        // Keycodes that continue Caps Word, without shifting.
        case KC_1 ... KC_0:
        case KC_BSPC:
        case KC_DEL:
        case DV_MINS:
        case DV_UNDS:
            return true;

        default:
            return false;  // Deactivate Caps Word.
    }
}

#ifdef OLED_ENABLE
oled_rotation_t oled_init_user(oled_rotation_t rotation) {
	return OLED_ROTATION_180;
}

static void render_kyria_logo(void) {
    static const char PROGMEM kyria_logo[] = {
        0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,128,128,192,224,240,112,120, 56, 60, 28, 30, 14, 14, 14,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7, 14, 14, 14, 30, 28, 60, 56,120,112,240,224,192,128,128,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
        0,  0,  0,  0,  0,  0,  0,192,224,240,124, 62, 31, 15,  7,  3,  1,128,192,224,240,120, 56, 60, 28, 30, 14, 14,  7,  7,135,231,127, 31,255,255, 31,127,231,135,  7,  7, 14, 14, 30, 28, 60, 56,120,240,224,192,128,  1,  3,  7, 15, 31, 62,124,240,224,192,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
        0,  0,  0,  0,240,252,255, 31,  7,  1,  0,  0,192,240,252,254,255,247,243,177,176, 48, 48, 48, 48, 48, 48, 48,120,254,135,  1,  0,  0,255,255,  0,  0,  1,135,254,120, 48, 48, 48, 48, 48, 48, 48,176,177,243,247,255,254,252,240,192,  0,  0,  1,  7, 31,255,252,240,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
        0,  0,  0,255,255,255,  0,  0,  0,  0,  0,254,255,255,  1,  1,  7, 30,120,225,129,131,131,134,134,140,140,152,152,177,183,254,248,224,255,255,224,248,254,183,177,152,152,140,140,134,134,131,131,129,225,120, 30,  7,  1,  1,255,255,254,  0,  0,  0,  0,  0,255,255,255,  0,  0,  0,  0,255,255,  0,  0,192,192, 48, 48,  0,  0,240,240,  0,  0,  0,  0,  0,  0,240,240,  0,  0,240,240,192,192, 48, 48, 48, 48,192,192,  0,  0, 48, 48,243,243,  0,  0,  0,  0,  0,  0, 48, 48, 48, 48, 48, 48,192,192,  0,  0,  0,  0,  0,
        0,  0,  0,255,255,255,  0,  0,  0,  0,  0,127,255,255,128,128,224,120, 30,135,129,193,193, 97, 97, 49, 49, 25, 25,141,237,127, 31,  7,255,255,  7, 31,127,237,141, 25, 25, 49, 49, 97, 97,193,193,129,135, 30,120,224,128,128,255,255,127,  0,  0,  0,  0,  0,255,255,255,  0,  0,  0,  0, 63, 63,  3,  3, 12, 12, 48, 48,  0,  0,  0,  0, 51, 51, 51, 51, 51, 51, 15, 15,  0,  0, 63, 63,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 48, 48, 63, 63, 48, 48,  0,  0, 12, 12, 51, 51, 51, 51, 51, 51, 63, 63,  0,  0,  0,  0,  0,
        0,  0,  0,  0, 15, 63,255,248,224,128,  0,  0,  3, 15, 63,127,255,239,207,141, 13, 12, 12, 12, 12, 12, 12, 12, 30,127,225,128,  0,  0,255,255,  0,  0,128,225,127, 30, 12, 12, 12, 12, 12, 12, 12, 13,141,207,239,255,127, 63, 15,  3,  0,  0,128,224,248,255, 63, 15,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
        0,  0,  0,  0,  0,  0,  0,  3,  7, 15, 62,124,248,240,224,192,128,  1,  3,  7, 15, 30, 28, 60, 56,120,112,112,224,224,225,231,254,248,255,255,248,254,231,225,224,224,112,112,120, 56, 60, 28, 30, 15,  7,  3,  1,128,192,224,240,248,124, 62, 15,  7,  3,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
        0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  1,  1,  3,  7, 15, 14, 30, 28, 60, 56,120,112,112,112,224,224,224,224,224,224,224,224,224,224,224,224,224,224,224,224,112,112,112,120, 56, 60, 28, 30, 14, 15,  7,  3,  1,  1,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0
    };
    oled_write_raw_P(kyria_logo, sizeof(kyria_logo));
}

static void render_qmk_logo(void) {
  static const char PROGMEM qmk_logo[] = {
    0x80,0x81,0x82,0x83,0x84,0x85,0x86,0x87,0x88,0x89,0x8a,0x8b,0x8c,0x8d,0x8e,0x8f,0x90,0x91,0x92,0x93,0x94,
    0xa0,0xa1,0xa2,0xa3,0xa4,0xa5,0xa6,0xa7,0xa8,0xa9,0xaa,0xab,0xac,0xad,0xae,0xaf,0xb0,0xb1,0xb2,0xb3,0xb4,
    0xc0,0xc1,0xc2,0xc3,0xc4,0xc5,0xc6,0xc7,0xc8,0xc9,0xca,0xcb,0xcc,0xcd,0xce,0xcf,0xd0,0xd1,0xd2,0xd3,0xd4,0};

  oled_write_P(qmk_logo, false);
}

static void render_lower(void) {
	oled_write_P(PSTR("       Symbols\n\n"), false);
	oled_write_P(PSTR("! @ { } |     _ \\    \n"), false);
	oled_write_P(PSTR("# $ ( ) `   + - / * %\n"), false);
	oled_write_P(PSTR("% ^ [ ] ~   & = , . /\n"), false);
}

/*
static void render_lower_left(void) {
	oled_write_P(PSTR("Symbols\n\n"), false);
	oled_write_P(PSTR("  ! @ { } |\n"), false);
	oled_write_P(PSTR("  # $ ( ) `\n"), false);
	oled_write_P(PSTR("  % ^ [ ] ~\n\n"), false);
}

static void render_lower_right(void) {
	oled_write_P(PSTR("Symbols\n\n"), false);
	oled_write_P(PSTR("      _ \\\n"), false);
	oled_write_P(PSTR("    + - / * %\n"), false);
	oled_write_P(PSTR("    & = , . /\n\n"), false);
}
*/

static void render_status(void) {
    // QMK Logo and version information
    render_qmk_logo();
    oled_write_P(PSTR("Kyria rev1.0\n\n"), false);

    // Host Keyboard Layer Status
    oled_write_P(PSTR("Layer: "), false);
    switch (get_highest_layer(layer_state)) {
        case _DVORAK:
            oled_write_P(PSTR("Dvorak\n"), false);
            break;
        case _LOWER:
            oled_write_P(PSTR("Lower\n"), false);
            break;
        case _RAISE:
            oled_write_P(PSTR("Raise\n"), false);
            break;
        case _FRENCH:
			oled_write_P(PSTR("French\n"), false);
            break;
        case _GAMING:
			oled_write_P(PSTR("Gaming\n"), false);
            break;
        default:
            oled_write_P(PSTR("Undefined\n"), false);
    }

    // Host Keyboard LED Status
    // uint8_t led_usb_state = host_keyboard_leds();
    led_t led_state = host_keyboard_led_state();
    oled_write_P(led_state.num_lock ? PSTR("NUMLCK ") : PSTR("       "), false);
    oled_write_P(led_state.caps_lock ? PSTR("CAPLCK ") : PSTR("       "), false);
    oled_write_P(led_state.scroll_lock ? PSTR("SCRLCK ") : PSTR("       "), false);
}

bool oled_task_user(void) {
    if (is_keyboard_master()) {
		if (get_highest_layer(layer_state) == _LOWER) {
			render_lower();
		} else {
			render_status(); // Renders the current keyboard state (layer, lock, caps, scroll, etc)
		}
    } else {
		render_kyria_logo();
    }
	return false;
}
#endif

#ifdef ENCODER_ENABLE
bool encoder_update_user(uint8_t index, bool clockwise) {
    if (index == 0) {
        // Volume control
        if (clockwise) {
            tap_code(KC_UP);
            tap_code(KC_UP);
            tap_code(KC_UP);
        } else {
            tap_code(KC_DOWN);
            tap_code(KC_DOWN);
            tap_code(KC_DOWN);
        }
    }
    else if (index == 1) {
        // Brightness control
        if (clockwise) {
            tap_code(KC_BRID);
            tap_code(KC_BRID);
        } else {
            tap_code(KC_BRIU);
            tap_code(KC_BRIU);
        }
    }
	return false;
}
#endif
