OLED_ENABLE = yes
OLED_DRIVER = ssd1306   # Enables the use of OLED displays
ENCODER_ENABLE = yes       # Enables the use of one or more encoders
RGBLIGHT_ENABLE = no      # Enable keyboard RGB underglow
MOUSEKEY_ENABLE = no
UNICODEMAP_ENABLE = yes
CAPS_WORD_ENABLE = yes
NO_USB_STARTUP_CHECK = yes

BOOTLOADER = caterina
