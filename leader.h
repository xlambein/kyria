#include QMK_KEYBOARD_H
#include "keymap_dvorak.h"
#include "sendstring_dvorak.h"

typedef struct {
  uint8_t keycode;
  uint8_t offset;
} cell_t;
typedef void (*leader_seq_t)(void);

enum leader_seqs {
  FOO,
  FISH,
};

// Trie of leader keys
#define CELL(...) (const cell_t[]) { __VA_ARGS__ {0, 0} }
static const cell_t *leader_cells[] = {
  CELL( {DV_F, 1}, ),
  CELL( {DV_O, 2}, {DV_I, 3}, ), // F
  CELL( {DV_O, 4}, ), // FO
  CELL( {DV_S, 5}, ), // FI
  CELL( {1, FOO}, ), // FOO
  CELL( {DV_H, 6}, ), // FIS
  CELL( {1, FISH}, ), // FISH
};

void run_seq(uint8_t seq) {
  switch (seq) {
    case FOO:
      SEND_STRING("bar");
      break;
    case FISH:
      tap_code16(G(DV_T));
      break;
  }
}

const cell_t* find_next_cell(const cell_t *cells, uint8_t keycode) {
  // Find the cell corresponding to this key
  while (cells->keycode != keycode) {
    // There's no cell for this key => abort
    if (cells->keycode == 0)
      return 0;
    cells++;
  }
  // Return the location of the next cell
  return leader_cells[cells->offset];
}

bool handle_leader_key(uint16_t keycode) {
  static const cell_t* current_cell = 0;
  // If the key is leader, start reading the next keys
  if (keycode == KC_LEAD) {
    current_cell = &leader_cells[0][0];
    return false;
  }
  // Otherwise, if we're already reading a leader sequence,
  // find the next cell corresponding to this key
  else if (current_cell != 0) {
    current_cell = find_next_cell(current_cell, keycode);
    // If we reached the end of a sequence, run it
    if (current_cell->keycode == 1) {
      run_seq(current_cell->offset);
      current_cell = 0;
    }
    return false;
  }
  return true;
}
